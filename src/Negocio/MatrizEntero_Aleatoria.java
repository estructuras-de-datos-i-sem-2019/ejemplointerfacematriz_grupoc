/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;

/**
 *
 * @author madar
 */
public class MatrizEntero_Aleatoria  implements IMatriz{
    
    private int matriz[][];

    /**
     * Constructor vacío
     */
    public MatrizEntero_Aleatoria() {
        //NO es necesario, sólo por definición:
        this.matriz=null;
    }
    
    
    /**
     *  Crea una matriz cuadrada o rectangular
     * @param n cantidad de filas
     * @param m  cantidad de columnas
     */
    public MatrizEntero_Aleatoria(int n, int m) throws Exception
    {
    if(n<=0 || m<=0)
        throw new Exception("No se puede crear matrices con tamaños negativos o cero");
    this.matriz=new int[n][m];
    }       
    
    /**
     * Método que crear una matriz aleatorio
     * @param limInicial valor inicial de generación del aleatorio
     * @param limFinal valor final de generación del aleatorio
     */
    public void crearMatriz(int limInicial, int limFinal) throws Exception
    {
    if (limInicial>=limFinal)
        throw new Exception("No se puede llenar la matriz, sus límites están fuera del intervalo");
    
    for(int i=0;i<this.matriz.length;i++)
    {
        //matriz[..][...]
        for(int j=0;j<this.matriz[i].length;j++)
        {
        this.matriz[i][j]=(int) Math.floor(Math.random()*(limInicial-limFinal+1)+limFinal);  
        }
    }
    }
    
    
public String toString()
{

    if(this.matriz==null)
        return "Matriz vacía";
    String msg="";
    for(int fila_vector[]:this.matriz)
    {
        for(int dato_columna:fila_vector)
            msg+=dato_columna+"\t";
     msg+="\n";
    }
return msg;
    
}


    @Override
    public int getSumaTotal() {
        if(this.matriz==null)
        return 0;
        
     int total=0;
     for(int fila_vector[]:this.matriz)
    {
        for(int dato_columna:fila_vector)
            total+=dato_columna;
     
    }
    
     return total;
    }
    
}
